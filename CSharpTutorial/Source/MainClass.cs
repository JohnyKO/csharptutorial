﻿using System;
using CSharpTutorial.MyLib;

namespace CSharpTutorial.Source
{
    class AnyClass
    {
        public static void Main()
        {
            Console.WriteLine("Its created from debag branch");
            Console.WriteLine("Create new User");
            User user = new User();

            Console.Write("Login: ");
            user.login = Console.ReadLine();
            Console.Write("Password: ");
            user.password = Console.ReadLine();
            Console.Write("Age: ");
            user.age = Convert.ToInt32(Console.ReadLine());

            //when we write just user - compilator calls user.ToString() method
            Console.WriteLine("Result " + user);

            Console.ReadLine();
        }
    }
}
