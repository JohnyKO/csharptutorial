﻿using System;

namespace CSharpTutorial.MyLib
{
    //Its created from debag branch
    class User
    {
        // set rules for change
        public string login { get; set; }
        // free access to the class field
        public string password { get; set; }
        // free access too
        public int age { get; set; }

        public User()
        {
            this.login = String.Empty;
            this.password = String.Empty;
        }

        public User(string login, string password)
        {
            this.login = login;
            this.password = password;
        }

        public override string ToString()
        {
            return "login: " + this.login + ", password: " + this.password;
        }
    }
}
