﻿using System;

namespace CSharpTutorial.MyLib
{
    //Its created from debag branch...
    //Test
    class MyMath
    {
        public static double Sum(double a, double b)
        {
            return a + b;
        }

        public static double Sub(double a, double b)
        {
            return a - b;
        }

        public static double Mult(double a, double b)
        {
            return a * b;
        }

        public static double Div(double a, double b)
        {
            if(b == 0)
            {
                throw new Exception("Divided by zero");
            }

            return a / b;
        }
    }
}
